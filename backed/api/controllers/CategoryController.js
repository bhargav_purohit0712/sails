/**
 * CategoryController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {

  show: async (req, res, next) => {
    const page = req.query.page
    if (page == 0) {
      const count = await Category.count()
      Category.find({})
        .paginate(page, 2)
        .exec((err, data) => {
          if (err) {
            res
              .status(500)
              .json({ err: 'Database Error', status: '500', msg: err })
          }
          res.status(200).json({ data: data, count: count })
        })
    } else {
      Category.find({})
        .paginate(page, 2)
        .exec((err, data) => {
          if (err) {
            res
              .status(500)
              .json({ err: 'Database Error', status: '500', msg: err })
          }
          res.status(200).json({ data: data })
        })
    }
  },

  add: (req, res, next) => {
    const name = req.body.name
    const desc = req.body.desc
    const status = req.body.status

    Category.create({
      name: name,
      desc: desc,
      status: status
    }).exec((err, data) => {
      if (err) {
        res.json({ err: 'Database Error', status: '500', msg: "name should be unique" })
      }
      res.json({ msg: 'data inserted' })
    })
  },

  delete: (req, res, next) => {
    const id = req.params.id
    Category.destroy({ id: id }).exec((err, data) => {
      if (err) {
        console.log(err)
        res.json({ err: 'Database Error', status: '500', msg: err })
      }
      res.json({ msg: 'deleted sus' })
    })
  },

  findCategory: (req, res, next) => {
    const id = req.params.id
    Category.findOne({ id: id }).exec((err, data) => {
      if (err) {
        console.log(err)
        res.json({ err: 'Database Error', status: '500', msg: err })
      }
      res.json({ data: data })
    })
  },
  
  update: (req, res, next) => {
    const id = req.body.id
    const name = req.body.name
    const desc = req.body.desc
    const status = req.body.status

    Category.update(
      { id: id },
      { name: name, desc: desc, status: status }
    ).exec((err, data) => {
      if (err) {
        console.log(err)
        res.json({ err: 'Database Error', status: '500', msg: err })
      }
      res.json({ msg: 'updated' })
    })
  }
}
