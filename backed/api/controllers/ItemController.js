

module.exports = {
  show: async (req, res, next) => {
    const page = req.query.page
    if (page == 0) {
      const count = await Item.count()
      Item.find({})
        .paginate(page, 2)
        .populate('cats')
        .exec((err, data) => {
          if (err) {
            res.json({ err: 'Database Error', status: '500', msg: err })
          }
          res.json({ data: data, count: count })
        })
    } else {
      Item.find({})
        .paginate(page, 2)
        .populate('cats')
        .exec((err, data) => {
          if (err) {
            res.json({ err: 'Database Error', status: '500', msg: err })
          }
          res.json({ data: data })
        })
    }
  },

  add: (req, res, next) => {
    const name = req.body.name
    const desc = req.body.desc
    const status = req.body.status
    const catid = req.body.catid
    const price = req.body.price
    Item.create({
      name: name,
      desc: desc,
      status: status,
      price: price,
      cats: catid
    }).exec((err, data) => {
      if (err) {
        console.log(err)
        res.json({ err: 'Database Error', status: '500', msg: err })
      }
      res.json({ msg: 'data inserted' })
    })
  },
  delete: (req, res, next) => {
    const id = req.params.id
    Item.destroy({ id: id }).exec((err, data) => {
      if (err) {
        console.log(err)
        res.json({ err: 'Database Error', status: '500', msg: err })
      }
      res.json({ msg: 'deleted sus' })
    })
  },
  findItem: (req, res, next) => {
    const id = req.params.id
    Item.findOne({ id: id }).exec((err, data) => {
      if (err) {
        console.log(err)
        res.json({ err: 'Database Error', status: '500', msg: err })
      }
      res.json({ data: data })
    })
  },
  update: (req, res, next) => {
    const id = req.body.id
    const name = req.body.name
    const desc = req.body.desc
    const status = req.body.status
    const cat = req.body.cat
    const price = req.body.price

    Item.update(
      { id: id },
      { name: name, desc: desc, status: status, price: price, cats: cat }
    ).exec((err, data) => {
      if (err) {
        console.log(err)
        res.json({ err: 'Database Error', status: '500', msg: err })
      }
      res.json({ msg: 'updated' })
    })
  }
}
