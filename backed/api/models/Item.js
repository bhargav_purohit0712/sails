/**
 * Item.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  attributes: {
    name: {
      type: 'string'
    },
    desc: {
      type: 'string'
    },
    status: {
      type: 'boolean'
    },
    price: {
      type: 'number'
    },
    cats: {
      model: 'category',
    }
  }
}
