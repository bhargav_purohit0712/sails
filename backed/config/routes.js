/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes tell Sails what to do each time it receives a request.
 *
 * For more information on configuring custom routes, check out:
 * https://sailsjs.com/anatomy/config/routes-js
 */

const catController = require('../api/controllers/CategoryController')
const itemController = require('../api/controllers/ItemController')

module.exports.routes = {

  'GET /api/cat': catController.show,
  'POST /api/cat': catController.add,
  'DELETE /api/cat/:id': catController.delete,
  'GET /api/findcat/:id': catController.findCategory,
  'PUT /api/cat': catController.update,

  'GET /api/item': itemController.show,
  'POST /api/item': itemController.add,
  'DELETE /api/item/:id': itemController.delete,
  'GET /api/finditem/:id': itemController.findItem,
  'PUT /api/item': itemController.update

}
