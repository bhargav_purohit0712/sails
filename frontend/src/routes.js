/*!

=========================================================
* Argon Dashboard React - v1.1.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/argon-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/

import addCat from 'views/examples/addCat.js'
import Cat from 'views/examples/category.js'
import addItem from 'views/examples/addItem.js'
import Item from 'views/examples/items.js'

var routes = [
  {
    path: '/index',
    name: 'Category',
    icon: 'ni ni-tv-2 text-primary',
    component: Cat,
    layout: '/admin'
  },
  {
    path: '/addCat',
    name: 'addCat',
    icon: 'ni ni-planet text-blue',
    component: addCat,
    layout: '/admin'
  },
  {
    path: '/items',
    name: 'Items',
    icon: 'ni ni-tv-2 text-primary',
    component: Item,
    layout: '/admin'
  },
  {
    path: '/addItem',
    name: 'addItem',
    icon: 'ni ni-planet text-blue',
    component: addItem,
    layout: '/admin'
  }
]
export default routes
