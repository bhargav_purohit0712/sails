import React from 'react'

import EditCat from '../../components/modal/catModal'

import Error from '../../components/error/error'

// reactstrap components
import {
  Spinner,
  Card,
  CardHeader,
  Table,
  Container,
  Row,
  Button,
  CardFooter,
  PaginationLink,
  Pagination,
  PaginationItem
} from 'reactstrap'
// core components
import Header from 'components/Headers/Header.js'

class Tables extends React.Component {
  state = {
    data: [],
    loading: true,
    modalShow: false,
    cid: '',
    page: 0,
    totalItem: 0,
    lastPage: 0,
    err: false,
    errMsg: '',
    sus: false,
    susMsg: ''
  }
  componentDidMount () {
    fetch('http://localhost:1337/api/cat/?page=' + 0, { method: 'GET' }).then(
      res =>
        res
          .json()
          .then(data => {
            if (data.err) {
              this.setState(() => ({
                err: true,
                errMsg: data.err,
                sus: false,
                susMsg: '',
                loading: false
              }))
            } else {
              this.setState(() => ({
                data: data.data,
                loading: false,
                totalItem: data.count,
                lastPage: Math.ceil(data.count / 2)
              }))
            }
          })
          .catch(E => {
            this.setState(() => ({
              err: true,
              errMsg: 'somthing wents wrong',
              sus: false,
              susMsg: '',
              loading: false
            }))
          })
    )
  }

  render () {
    
    const editCat = id => {
      this.setState({ cid: id, modalShow: true })
    }

    const loadPostBack = () => {
      let cp = this.state.page
      cp--
      if (cp >= 0) {
        fetch('http://localhost:1337/api/cat/?page=' + cp, {
          method: 'GET'
        }).then(res =>
          res
            .json()
            .then(data => {
              this.setState(() => ({ data: data.data, page: cp }))
            })
            .catch(E => {
              this.setState(() => ({
                err: true,
                errMsg: 'somthing wents wrong',
                sus: false,
                susMsg: '',
                loading: false
              }))
            })
        )
      }
    }

    const loadPostFor = () => {
      let cp = this.state.page
      cp++
      if (cp < this.state.lastPage) {
        fetch('http://localhost:1337/api/cat/?page=' + cp, {
          method: 'GET'
        }).then(res =>
          res
            .json()
            .then(data => {
              this.setState(() => ({ data: data.data, page: cp }))
            })
            .catch(E => {
              this.setState(() => ({
                err: true,
                errMsg: 'somthing wents wrong',
                sus: false,
                susMsg: '',
                loading: false
              }))
            })
        )
      }
    }

    const delCat = id => {
      this.setState(() => ({ loading: false }))

      fetch('http://localhost:1337/api/cat/' + id, { method: 'DELETE' })
        .then(res => {
          fetch('http://localhost:1337/api/cat', { method: 'GET' }).then(res =>
            res
              .json()
              .then(data => {
                this.setState(() => ({ data: data.data, loading: false }))
              })
              .catch(E => {
                this.setState(() => ({
                  err: true,
                  errMsg: 'somthing wents wrong',
                  sus: false,
                  susMsg: '',
                  loading: false
                }))
              })
          )
        })
        .catch(E => {
          this.setState(() => ({
            err: true,
            errMsg: 'somthing wents wrong',
            sus: false,
            susMsg: '',
            loading: false
          }))
        })
    }

    const tb = this.state.data.length ? (
      this.state.data.map(sd => {
        return (
          <tr key={sd.id}>
            <th scope='row'>{sd.id}</th>
            <td>{sd.name}</td>
            <td>{sd.desc.slice(0, 20)}..</td>
            <td>{sd.status ? 'active' : 'inactive'}</td>
            <td>
              <Button color='info' onClick={() => editCat(sd.id)}>
                Edit
              </Button>{' '}
              <Button color='danger' onClick={() => delCat(sd.id)}>
                Delete
              </Button>
            </td>
          </tr>
        )
      })
    ) : (
      <tr>
        <td>
          <h2>NO CATEGORY YET!!!</h2>
        </td>
      </tr>
    )
    return this.state.loading ? (
      <Spinner color='primary' />
    ) : (
      <>
        <Header />
        {/* Page content */}
        <Container className='mt--7' fluid>
          <Error
            err={this.state.err}
            errMsg={this.state.errMsg}
            sus={this.state.sus}
            susMsg={this.state.susMsg}
          />
          {/* Table */}
          <Row>
            <div className='col'>
              <Card className='shadow'>
                <CardHeader className='border-0'>
                  <h3 className='mb-0'>Category tables</h3>
                </CardHeader>
                <Table className='align-items-center table-flush' responsive>
                  <thead className='thead-light'>
                    <tr>
                      <th scope='col'>id</th>
                      <th scope='col'>name</th>
                      <th scope='col'>Description</th>
                      <th scope='col'>Status</th>
                      <th scope='col'>Action</th>
                      <th scope='col' />
                    </tr>
                  </thead>
                  <tbody>{tb}</tbody>
                </Table>
                <CardFooter>
                  <Pagination aria-label='Page navigation example'>
                    <PaginationItem>
                      <PaginationLink
                        previous
                        href='#'
                        onClick={() => loadPostBack()}
                      />
                    </PaginationItem>
                    <PaginationItem>
                      <PaginationLink
                        next
                        href='#'
                        onClick={() => loadPostFor()}
                      />
                    </PaginationItem>
                  </Pagination>
                </CardFooter>
              </Card>
            </div>
          </Row>
          {this.state.modalShow ? (
            <EditCat
              cid={this.state.cid}
              show={this.state.modalShow}
              onHide={() => this.setState({ modalShow: false })}
            />
          ) : null}
        </Container>
      </>
    )
  }
}

export default Tables
