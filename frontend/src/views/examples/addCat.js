import Header from 'components/Headers/Header'
import React, { useState } from 'react'
import Error from '../../components/error/error'

// reactstrap components
import {
  Form,
  FormGroup,
  Label,
  Input,
  Container,
  Row,
  Button
} from 'reactstrap'

const Login = () => {
  const [name, setName] = useState('')
  const [swtich, setSwtich] = useState(true)
  const [desc, setDesc] = useState('')

  const [err, setErr] = useState(false)
  const [errMsg, setErrMsg] = useState('')
  const [sus, setSus] = useState(false)
  const [susMsg, setSusMsg] = useState('')

  const addCat = e => {
    e.preventDefault()
    const data = {
      name: name,
      desc: desc,
      status: swtich
    }
    fetch('http://localhost:1337/api/cat', {
      method: 'POST',
      body: JSON.stringify(data)
    })
      .then(res =>
        res.json().then(res => {
          console.log(res)
          if (res.err) {
            setSusMsg(() => '')
            setSus(() => false)
            setErrMsg(() => res.msg)
            setErr(() => true)
          } else {
            setSusMsg(() => 'created..')
            setSus(() => true)
            setErrMsg(() => '')
            setErr(() => false)
            setName(() => '')
            setDesc(() => '')
          }
        })
      )
      .catch(e => {
        setErrMsg(() => 'somthing wents wrong')
        setErr(() => true)
        setSusMsg(() => '')
        setSus(() => false)
      })
  }

  return (
    <>
      <Header />
      <Container>
        <Error err={err} errMsg={errMsg} sus={sus} susMsg={susMsg} />
        <Form onSubmit={e => addCat(e)}>
          <FormGroup>
            <Label for='name'>name</Label>
            <Input
              type='text'
              name='desc'
              value={name}
              onChange={e => setName(e.target.value)}
              id='name'
              placeholder='Enter Category'
              required
            />
          </FormGroup>
          <FormGroup>
            <Label for='Desc'>Description</Label>

            <Input
              value={desc}
              onChange={e => setDesc(e.target.value)}
              type='textarea'
              placeholder='Enter Description'
              name='desc'
              id='desc'
              required
            />
          </FormGroup>
          <FormGroup>
            <Row>
              <Label
                for='Status'
                style={{ paddingRight: '20px', paddingLeft: '20px' }}
              >
                Status
              </Label>

              <label className='custom-toggle custom-toggle-primary'>
                <input
                  type='checkbox'
                  checked={swtich ? 'on' : ''}
                  onChange={() => setSwtich(old => !old)}
                />
                <span
                  className='custom-toggle-slider rounded-circle'
                  data-label-off='OFF'
                  data-label-on='ON'
                ></span>
              </label>
            </Row>
          </FormGroup>
          <Button>Add Category</Button>
        </Form>
      </Container>
    </>
  )
}

export default Login
