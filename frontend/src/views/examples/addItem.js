/*!

=========================================================
* Argon Dashboard React - v1.1.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/argon-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import Header from 'components/Headers/Header'
import React, { useState, useEffect } from 'react'
import { numberFormat } from '../../utils/currencyFilter'
import Error from '../../components/error/error'
import { useHistory } from 'react-router-dom'
// reactstrap components
import {
  Form,
  FormGroup,
  Label,
  Input,
  Container,
  Row,
  Button
} from 'reactstrap'

const Login = () => {
  const [name, setName] = useState('')
  const [swtich, setSwtich] = useState(true)
  const [desc, setDesc] = useState('')
  const [cat, setCat] = useState([])
  const [loading, setLoading] = useState(true)
  const [selectedCat, setSelectedCat] = useState('')
  const [price, setPrice] = useState(0)
  const [cPrice, setCPrice] = useState('')
  const [err, setErr] = useState(false)
  const [errMsg, setErrMsg] = useState('')
  const [sus, setSus] = useState(false)
  const [susMsg, setSusMsg] = useState('')
  const history = useHistory()

  useEffect(() => {
    fetch('http://localhost:1337/api/cat', { method: 'GET' }).then(res =>
      res
        .json()
        .then(data => {
          if (data.data.length) {
            setCat(() => [...data.data])

            setSelectedCat(data.data[0].id)
            setLoading(false)
          } else {
            history.replace('/admin/addCat')
          }
        })
        .catch(E => {
          setSusMsg(() => '')
          setSus(() => false)
          setErrMsg(() => 'somthing wents wrong..')
          setErr(() => true)
        })
    )
  }, [history])

  const catOp = !loading ? (
    cat.length ? (
      cat.map(a => (
        <option key={a.id} id={a.id} name={a.id} value={a.id}>
          {a.name}
        </option>
      ))
    ) : (
      <h1>Please Enter Cat 1st</h1>
    )
  ) : null

  const addCat = e => {
    e.preventDefault()
    const data = {
      name: name,
      desc: desc,
      status: swtich,
      catid: selectedCat,
      price: price
    }
    fetch('http://localhost:1337/api/item', {
      method: 'POST',
      body: JSON.stringify(data)
    })
      .then(res =>
        res.json().then(res => {
          console.log(res)
          if (res.err) {
            setSusMsg(() => '')
            setSus(() => false)
            setErrMsg(() => 'somthing wents wrong..')
            setErr(() => true)
          } else {
            setSusMsg(() => 'created..')
            setSus(() => true)
            setErrMsg(() => '')
            setErr(() => false)
            setDesc(() => '')
            setPrice(() => 0)
            setName(() => '')
          }
        })
      )
      .catch(e => {
        setErrMsg(() => 'somthing wents wrong')
        setErr(() => true)
        setSusMsg(() => '')
        setSus(() => false)
      })
  }

  return (
    <>
      <Header />
      <Container>
        <Error err={err} errMsg={errMsg} sus={sus} susMsg={susMsg} />

        <Form onSubmit={e => addCat(e)}>
          <FormGroup>
            <Label for='name'>name</Label>
            <Input
              type='text'
              name='desc'
              value={name}
              onChange={e => setName(e.target.value)}
              id='name'
              placeholder='Enter Category'
              required
            />
          </FormGroup>
          <FormGroup>
            <Label for='Desc'>Description</Label>

            <Input
              value={desc}
              onChange={e => setDesc(e.target.value)}
              type='textarea'
              placeholder='Enter Description'
              name='desc'
              id='desc'
              required
            />
          </FormGroup>
          <FormGroup>
            <Label for='Desc'>
              Price :- {'  '} {cPrice}
            </Label>

            <Input
              value={price}
              onChange={e => {
                setPrice(e.target.value)
                setCPrice(numberFormat(e.target.value))
              }}
              type='number'
              placeholder='Enter Price'
              name='price'
              id='price'
              required
            />
          </FormGroup>
          <FormGroup>
            <Row>
              <Label
                for='Status'
                style={{ paddingRight: '20px', paddingLeft: '20px' }}
              >
                Status
              </Label>

              <label className='custom-toggle custom-toggle-primary'>
                <input
                  type='checkbox'
                  checked={swtich ? 'on' : ''}
                  onChange={() => setSwtich(old => !old)}
                />
                <span
                  className='custom-toggle-slider rounded-circle'
                  data-label-off='OFF'
                  data-label-on='ON'
                ></span>
              </label>
            </Row>
          </FormGroup>
          <FormGroup>
            <Label for='Category'>Category</Label>
            <Input
              type='select'
              name='select'
              id='cat'
              onChange={e => setSelectedCat(e.target.value)}
            >
              {catOp}
            </Input>
          </FormGroup>
          <Button>Add Item</Button>
        </Form>
      </Container>
    </>
  )
}

export default Login
