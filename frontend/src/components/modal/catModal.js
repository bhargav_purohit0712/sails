import React, { useEffect, useState, useRef } from 'react'

import { Modal } from 'react-bootstrap'

import { Spinner, Form, FormGroup, Label, Input, Row, Button } from 'reactstrap'

import { useHistory } from 'react-router-dom'

const Editccount = props => {
  const [name, setName] = useState('')
  const [swtich, setSwtich] = useState(true)
  const [desc, setDesc] = useState('')
  const [loading, setLoading] = useState(true)
  const [id, setId] = useState('')
  const history = useHistory()
  const cid = useRef(props.cid)

  const updateCat = () => {
    const data = {
      id: id,
      desc: desc,
      name: name,
      status: swtich
    }
    fetch('http://localhost:1337/api/cat', {
      method: 'PUT',
      body: JSON.stringify(data)
    })
      .then(res => {
        history.replace('admin')
      })
      .catch(e => console.log(e))
  }
  useEffect(() => {
    fetch('http://localhost:1337/api/findcat/' + cid.current, {
      method: 'GET'
    }).then(res =>
      res
        .json()
        .then(data => {
          setName(() => data.data.name)
          setDesc(() => data.data.desc)
          setId(() => data.data.id)
          setLoading(false)
        })
        .catch(E => console.log(E))
    )
  }, [])

  return loading ? (
    <Spinner />
  ) : (
    <Modal
      show={props.show}
      size='xl'
      aria-labelledby='contained-modal-title-vcenter'
      backdrop='static'
      centered
    >
      <Modal.Header>
        <Modal.Title>Update Category</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Form>
          <FormGroup>
            <Label for='name'>name</Label>
            <Input
              type='text'
              name='desc'
              value={name}
              onChange={e => setName(e.target.value)}
              id='name'
              placeholder='Enter Category'
              required
            />
          </FormGroup>
          <FormGroup>
            <Label for='Desc'>Description</Label>

            <Input
              value={desc}
              onChange={e => setDesc(e.target.value)}
              type='textarea'
              placeholder='Enter Description'
              name='desc'
              id='desc'
              required
            />
          </FormGroup>
          <FormGroup>
            <Row>
              <Label
                for='Status'
                style={{ paddingRight: '20px', paddingLeft: '20px' }}
              >
                Status
              </Label>

              <label className='custom-toggle custom-toggle-primary'>
                <input
                  type='checkbox'
                  checked={swtich ? 'on' : ''}
                  onChange={() => setSwtich(old => !old)}
                />
                <span
                  className='custom-toggle-slider rounded-circle'
                  data-label-off='OFF'
                  data-label-on='ON'
                ></span>
              </label>
            </Row>
          </FormGroup>
        </Form>
      </Modal.Body>
      <Modal.Footer>
        <Button onClick={() => updateCat()}>Update Category</Button>
        <Button
          onClick={() => {
            history.replace('/admin')
          }}
        >
          close
        </Button>
      </Modal.Footer>
    </Modal>
  )
}

export default Editccount
