import React, { useEffect, useState, useRef } from 'react'

import { Modal } from 'react-bootstrap'

import { Spinner, Form, FormGroup, Label, Input, Row, Button } from 'reactstrap'

import { useHistory } from 'react-router-dom'

import { numberFormat } from '../../utils/currencyFilter'

const Editccount = props => {
  const [name, setName] = useState('')
  const [swtich, setSwtich] = useState(true)
  const [desc, setDesc] = useState('')
  const [cat, setCat] = useState([])
  const history = useHistory()
  const [loading, setLoading] = useState(true)
  const [id, setId] = useState('')
  const [selectedCat, setSelectedCat] = useState('')
  const [price, setPrice] = useState(0)
  const [covertedp, setCP] = useState('')
  const iid = useRef(props.iid)
  const updateCat = () => {
    const data = {
      id: id,
      desc: desc,
      name: name,
      status: swtich,
      price: price,
      cat: selectedCat
    }
    fetch('http://localhost:1337/api/item', {
      method: 'PUT',
      body: JSON.stringify(data)
    })
      .then(res => {
        history.replace('/admin/items')
      })
      .catch(e => console.log(e))
  }
  useEffect(() => {
    fetch('http://localhost:1337/api/cat', { method: 'GET' }).then(res =>
      res
        .json()
        .then(data => {
          if (data.data.length) {
            setCat(() => [...data.data])

            setSelectedCat(data.data[0].id)
            fetch('http://localhost:1337/api/finditem/' + iid.current, {
              method: 'GET'
            }).then(res =>
              res
                .json()
                .then(data => {
                  setName(() => data.data.name)
                  setDesc(() => data.data.desc)
                  setPrice(() => data.data.price)
                  setId(() => data.data.id)
                  setSwtich(() => data.data.status)
                  setCP(() => numberFormat(data.data.price))
                  setLoading(false)
                })
                .catch(E => console.log(E))
            )
          } else {
            history.replace('/admin/addCat')
          }
        })
        .catch(E => console.log(E))
    )
  }, [history])

  const catOp = !loading ? (
    cat.length ? (
      cat.map(a => (
        <option key={a.id} id={a.id} name={a.id} value={a.id}>
          {a.name}
        </option>
      ))
    ) : (
      <h1>Please Enter Cat 1st</h1>
    )
  ) : null

  return loading ? (
    <Spinner />
  ) : (
    <Modal
      show={props.show}
      size='xl'
      aria-labelledby='contained-modal-title-vcenter'
      backdrop='static'
      centered
    >
      <Modal.Header>
        <Modal.Title>Update Item</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Form>
          <FormGroup>
            <Label for='name'>name</Label>
            <Input
              type='text'
              name='desc'
              value={name}
              onChange={e => setName(e.target.value)}
              id='name'
              placeholder='Enter Category'
              required
            />
          </FormGroup>
          <FormGroup>
            <Label for='Desc'>Description</Label>

            <Input
              value={desc}
              onChange={e => setDesc(e.target.value)}
              type='textarea'
              placeholder='Enter Description'
              name='desc'
              id='desc'
              required
            />
          </FormGroup>
          <FormGroup>
            <Label for='Desc'>Price:-{covertedp}</Label>

            <Input
              value={price}
              onChange={e => setPrice(e.target.value)}
              type='number'
              placeholder='Enter Price'
              name='price'
              id='price'
              required
            />
          </FormGroup>
          <FormGroup>
            <Row>
              <Label
                for='Status'
                style={{ paddingRight: '20px', paddingLeft: '20px' }}
              >
                Status
              </Label>

              <label className='custom-toggle custom-toggle-primary'>
                <input
                  type='checkbox'
                  checked={swtich ? 'on' : ''}
                  onChange={() => setSwtich(old => !old)}
                />
                <span
                  className='custom-toggle-slider rounded-circle'
                  data-label-off='OFF'
                  data-label-on='ON'
                ></span>
              </label>
            </Row>
          </FormGroup>
          <FormGroup>
            <Label for='Category'>Category</Label>
            <Input
              type='select'
              name='select'
              id='cat'
              onChange={e => setSelectedCat(e.target.value)}
            >
              {catOp}
            </Input>
          </FormGroup>
        </Form>
      </Modal.Body>
      <Modal.Footer>
        <Button onClick={() => updateCat()}>Update Category</Button>
        <Button
          onClick={() => {
            history.replace('/admin')
          }}
        >
          close
        </Button>
      </Modal.Footer>
    </Modal>
  )
}

export default Editccount
